#!/bin/bash
TESTS=${@-'django_vuejs'}
SETTINGS_FILE=${@-'django_vuejs.settings.ci'}

python ./manage.py test $TESTS --settings=$SETTINGS_FILE