¿Cómo usar el entorno de development? 
=================

1) Instalar docker y docker-compose https://docs.docker.com/compose/install/
2) Ejecutar el siguiente comando que descarga/crea el container y corre el proyecto DJANGO:

$ cd ~/ruta/django_vuejs && sudo docker-compose up



Comandos útiles
=================

Parar ejecución de los containers y eliminarlos:

    $ docker-compose down

Entrar al container:
   
    $ docker exec -it id_container /bin/bash

Listar containers:
   
    $ docker ps -a

Listar imágenes:
   
    $ docker images

Eliminar imagen:

    $ docker rmi id-imagen

