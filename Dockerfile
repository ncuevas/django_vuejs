FROM python
ENV PYTHONUNBUFFERED 1
RUN /bin/bash -c "mkdir /code"
WORKDIR /code/django_vuejs

ENV NVM_DIR /usr/local/nvm
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
ENV NODE_VERSION v8.9.4
RUN /bin/bash -c "source $NVM_DIR/nvm.sh && nvm install $NODE_VERSION && nvm use --delete-prefix $NODE_VERSION"
ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH

RUN /bin/bash -c "echo 'export NVM_DIR=/usr/local/nvm' >> /etc/environment"
RUN /bin/bash -c "echo 'export NODE_PATH=$NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules' >> /etc/environment"
RUN /bin/bash -c "echo 'export PATH=$NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH' >> /etc/environment"
RUN /bin/bash -c "echo 'source $NVM_DIR' >> /etc/environment"

RUN npm install -g vue-cli yarn

ADD ./ /code/django_vuejs