#!/usr/bin/env bash
projectroot=$PWD
pip install -r requirements/dev.txt
python manage.py migrate
cd django_vuejs/apps/book/frontend/ && yarn install && npm run build
cd $projectroot
python manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:8000
