from django.conf.urls import url, include
from tastypie.api import Api
from django_vuejs.apps.rest_api.v1 import *

v1_api = Api(api_name='v1')
v1_api.register(LibraryResource())
v1_api.register(AuthorResource())
v1_api.register(BookResource())
v1_api.register(UserResource())

urlpatterns = [
    url(r'^api/', include(v1_api.urls)),
]
