from tastypie import fields
from tastypie.serializers import Serializer
from tastypie.paginator import Paginator
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization

from django_vuejs.apps.book.models import Book
from django_vuejs.apps.rest_api.v1 import AuthorResource, LibraryResource
from django_vuejs.apps.rest_api.authentication import *


class BookResource(ModelResource):
    """
    class BookResource: User Endpoint
          Allowed Methods: GET, POST, PUT, DELETE
    """
    author = fields.ForeignKey(
        AuthorResource, 'author', full=True, null=True, blank=True)
    libraries = fields.ToManyField(
        LibraryResource, 'libraries', full=True, null=True, blank=True)

    class Meta:
        queryset = Book.objects.all()
        resource_name = 'book'
        excludes = []
        serializer = Serializer()
        paginator_class = Paginator
        ordering = ['id', 'title']
        filtering = {
            'id': ALL,
            'title': ALL,
            'author': ALL_WITH_RELATIONS,
            'libraries': ALL_WITH_RELATIONS,
        }
        authentication = MultiAuthentication(SimpleJwtAuthentication())
        authorization = Authorization()
