from tastypie.serializers import Serializer
from tastypie.paginator import Paginator
from tastypie.resources import ModelResource, ALL
from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization

from django_vuejs.apps.book.models import Library
from django_vuejs.apps.rest_api.authentication import *


class LibraryResource(ModelResource):
    """
    class LibraryResource: User Endpoint
          Allowed Methods: GET, POST, PUT, DELETE
    """

    class Meta:
        queryset = Library.objects.all()
        resource_name = 'library'
        excludes = []
        serializer = Serializer()
        paginator_class = Paginator
        ordering = ['id', 'first_name', 'last_name']
        filtering = {
            'id': ALL,
            'name': ALL,
        }
        authentication = MultiAuthentication(SimpleJwtAuthentication())
        authorization = Authorization()
