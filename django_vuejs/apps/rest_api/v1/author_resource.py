from tastypie.serializers import Serializer
from tastypie.paginator import Paginator
from tastypie.resources import ModelResource, ALL
from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from django_vuejs.apps.book.models import Author
from django_vuejs.apps.rest_api.authentication import *



class AuthorResource(ModelResource):
    """
    class AuthorResource: User Endpoint
          Allowed Methods: GET, POST, PUT, DELETE
    """

    class Meta:
        queryset = Author.objects.all()
        resource_name = 'author'
        excludes = []
        serializer = Serializer()
        paginator_class = Paginator
        ordering = ['id', 'first_name', 'last_name']
        filtering = {
            'id': ALL,
            'first_name': ALL,
            'last_name': ALL,
        }
        authentication = MultiAuthentication(SimpleJwtAuthentication())
        authorization = Authorization()
