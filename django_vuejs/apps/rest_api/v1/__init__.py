from django_vuejs.apps.rest_api.v1.library_resource import LibraryResource
from django_vuejs.apps.rest_api.v1.author_resource import AuthorResource
from django_vuejs.apps.rest_api.v1.book_resource import BookResource

from django_vuejs.apps.rest_api.v1.user_resource import UserResource
