import json
from django.contrib.auth.models import User
from django.conf.urls import *
from django.db import IntegrityError
from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django_vuejs.apps.rest_api.forms import UserForm
from django_vuejs.apps.rest_api.authentication import *


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_superuser']
        authentication = MultiAuthentication(SimpleJwtAuthentication())
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/auth%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('auth'), name="api_user_auth"),
        ]

    def obj_create(self, bundle, request=None, **kwargs):
        try:
            bundle = super(
                UserResource, self).obj_create(bundle, **kwargs)
            bundle.obj.set_password(bundle.data.get('password'))
            bundle.obj.save()
        except IntegrityError:
            raise BadRequest('That username already exists')
        return bundle

    def auth(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        user_data = json.loads(request.body)
        user_form = UserForm(user_data)
        response = self.create_response(
            request,
            {'error': 'INVALID_USER'},
            HttpUnauthorized
        )

        if user_form.is_valid():
            try:
                user = User.objects.get(username=user_data.get('username'))
                if user.check_password(user_data.get('password')):

                    fields = ['email', 'first_name', 'id',
                              'is_active', 'is_staff', 'last_name', 'username']
                    jwt = JwtAuth()
                    response_data = {
                        field: user.__dict__.get(field) for field in fields}
                    token = jwt.create_token(response_data)
                    response = self.create_response(
                        request,
                        {'user': response_data,
                         'token': token}
                    )
            except Exception as e:
                pass
        return response
