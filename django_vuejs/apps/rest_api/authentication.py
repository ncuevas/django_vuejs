from tastypie.authentication import Authentication
import django_vuejs.apps.rest_api.settings as rest_settings
from datetime import datetime, timedelta
import jwt


class JwtAuth():

    def __init__(self):
        self.secret_key = rest_settings.JWT_SETTINGS.get('SECRET')
        self.encrypt = rest_settings.JWT_SETTINGS.get('ALGORITHM')
        self.aud = None

    def create_token(self, data={}):
        time = datetime.utcnow() + timedelta(
            seconds=rest_settings.JWT_SETTINGS.get('EXPIRATION_TIME'))
        token = {
            'exp': time,
            'data': data
        }
        return jwt.encode(
            token,
            self.secret_key,
            algorithm=self.encrypt)

    def check_token(self, token=''):
        try:
            token_decoded = jwt.decode(
                token, self.secret_key, algorithms=self.encrypt)
        except Exception as e:
            raise e
        return token_decoded

    def get_data(self, token=''):
        try:
            token_decoded = jwt.decode(
                token, self.secret_key, algorithms=self.encrypt)
        except Exception as e:
            raise e
        return token_decoded


class SimpleJwtAuthentication(Authentication):

    def is_authenticated(self, request, **kwargs):
        try:
            token = request.META.get('HTTP_AUTHORIZATION')
            jwt_auth = JwtAuth()
            jwt_auth.check_token(token)
        except Exception:
            return False
        return True
