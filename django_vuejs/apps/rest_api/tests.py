from django.contrib.auth.models import User
from django.test import TestCase
from tastypie.test import ResourceTestCaseMixin, TestApiClient
from django_vuejs.apps.book.models import *
import json

API_PATH = '/api/v1/'


class UserResourceTest(ResourceTestCaseMixin, TestCase):

    def setUp(self):
        self.api_client = TestApiClient()
        self.first_name = 'test'
        self.last_name = 'test'
        self.username = 'test'
        self.password = 'test1234'
        self.email = 'test@test.com'
        self.user = User.objects.create_superuser(
            self.username,
            self.email,
            self.password,
            first_name=self.first_name,
            last_name=self.last_name)

        self.auth_data = {
            'username': self.username,
            'password': self.password,
        }
        self.user_data = {
            'first_name': 'homer',
            'username': 'test2',
            'password': 'BJ12cueG'
        }
        self.auth_invalid_data = {
            'username': self.username,
            'password': '3213213'
        }

    def get_jwt(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_data)
        data = json.loads(response._container[0])
        return data.get('token')

    def test_get_list_unauthenticated(self):
        self.assertHttpUnauthorized(
            self.api_client.get(
                '{0}{1}'.format(API_PATH, 'user/'), format='json'))

    def test_get_list(self):
        token = self.get_jwt()
        response = self.api_client.get(
            '{0}{1}'.format(API_PATH, 'user/'),
            format='json',
            HTTP_AUTHORIZATION=token)
        self.assertHttpOK(response)

    def test_post_auth(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_data)
        self.assertHttpOK(response)

    def test_post_auth_unauthorized(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_invalid_data)
        self.assertHttpUnauthorized(response)

    def test_post_create(self):
        token = self.get_jwt()
        self.assertEqual(User.objects.count(), 1)
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user'),
            format='json',
            data=self.user_data,
            HTTP_AUTHORIZATION=token)
        self.assertHttpCreated(response)
        self.assertEqual(User.objects.count(), 2)

    def test_post_update(self):
        token = self.get_jwt()
        self.api_client.put(
            '{0}{1}{2}'.format(API_PATH, 'user/', self.user.pk),
            format='json',
            data={'first_name': self.user_data.get('first_name')},
            HTTP_AUTHORIZATION=token)
        user_updated = User.objects.get(id=self.user.pk)
        self.assertEqual(user_updated.first_name, 'homer')


class LibraryResourceTest(ResourceTestCaseMixin, TestCase):

    def setUp(self):
        self.api_client = TestApiClient()
        self.first_name = 'test'
        self.last_name = 'test'
        self.username = 'test'
        self.password = 'test1234'
        self.email = 'test@test.com'
        self.user = User.objects.create_superuser(
            self.username,
            self.email,
            self.password,
            first_name=self.first_name,
            last_name=self.last_name)
        self.library = Library.objects.create(name="cavern")
        self.auth_data = {
            'username': self.username,
            'password': self.password,
        }

        self.object_data = {
            'name': 'simpsons',
        }

    def get_jwt(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_data)
        data = json.loads(response._container[0])
        return data.get('token')

    def test_get_list_unauthenticated(self):
        self.assertHttpUnauthorized(
            self.api_client.get(
                '{0}{1}'.format(API_PATH, 'library/'), format='json'))

    def test_get_list(self):
        token = self.get_jwt()
        response = self.api_client.get(
            '{0}{1}'.format(API_PATH, 'library/'),
            format='json',
            HTTP_AUTHORIZATION=token)
        self.assertHttpOK(response)

    def test_post_create_unauthorized(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'library'), format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION='xxxxxxxxxxxx')
        self.assertHttpUnauthorized(response)

    def test_post_create(self):
        token = self.get_jwt()
        self.assertEqual(Library.objects.count(), 1)
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'library'),
            format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION=token)
        self.assertHttpCreated(response)
        self.assertEqual(Library.objects.count(), 2)

    def test_post_update(self):
        token = self.get_jwt()
        self.api_client.put(
            '{0}{1}{2}'.format(API_PATH, 'library/', self.library.pk),
            format='json',
            data={'name': self.object_data.get('name')},
            HTTP_AUTHORIZATION=token)
        object_updated = Library.objects.get(id=self.library.pk)
        self.assertEqual(object_updated.name, 'simpsons')


class AuthorResourceTest(ResourceTestCaseMixin, TestCase):

    def setUp(self):
        self.api_client = TestApiClient()
        self.first_name = 'test'
        self.last_name = 'test'
        self.username = 'test'
        self.password = 'test1234'
        self.email = 'test@test.com'
        self.user = User.objects.create_superuser(
            self.username,
            self.email,
            self.password,
            first_name=self.first_name,
            last_name=self.last_name)
        self.author = Author.objects.create(
            first_name="ned", last_name="flanders")
        self.auth_data = {
            'username': self.username,
            'password': self.password,
        }

        self.object_data = {
            'first_name': 'homer',
            'last_name': 'simpson',
        }

    def get_jwt(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_data)
        data = json.loads(response._container[0])
        return data.get('token')

    def test_get_list_unauthenticated(self):
        self.assertHttpUnauthorized(
            self.api_client.get(
                '{0}{1}'.format(API_PATH, 'author/'), format='json'))

    def test_get_list(self):
        token = self.get_jwt()
        response = self.api_client.get(
            '{0}{1}'.format(API_PATH, 'author/'),
            format='json',
            HTTP_AUTHORIZATION=token)
        self.assertHttpOK(response)

    def test_post_create_unauthorized(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'author'), format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION='xxxxxxxxxxxx')
        self.assertHttpUnauthorized(response)

    def test_post_create(self):
        token = self.get_jwt()
        self.assertEqual(Author.objects.count(), 1)
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'author'),
            format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION=token)
        self.assertHttpCreated(response)
        self.assertEqual(Author.objects.count(), 2)

    def test_post_update(self):
        token = self.get_jwt()
        self.api_client.put(
            '{0}{1}{2}'.format(API_PATH, 'author/', self.author.pk),
            format='json',
            data={'first_name': self.object_data.get('first_name')},
            HTTP_AUTHORIZATION=token)
        object_updated = Author.objects.get(id=self.author.pk)
        self.assertEqual(object_updated.first_name, 'homer')


class BookResourceTest(ResourceTestCaseMixin, TestCase):

    def setUp(self):
        self.api_client = TestApiClient()
        self.first_name = 'test'
        self.last_name = 'test'
        self.username = 'test'
        self.password = 'test1234'
        self.email = 'test@test.com'
        self.user = User.objects.create_superuser(
            self.username,
            self.email,
            self.password,
            first_name=self.first_name,
            last_name=self.last_name)
        self.library = Library.objects.create(name="cavern")
        self.author = Author.objects.create(
            first_name="ned", last_name="flanders")
        self.book = Book.objects.create(
            title="jpluplus",
            author=self.author)
        self.book.libraries.add(self.library)
        self.book.save()
        self.auth_data = {
            'username': self.username,
            'password': self.password,
        }

        self.object_data = {
            'title': 'python',
            'author': {'pk':self.author.pk},
            'library': {'pk':self.library.pk}
        }

    def get_jwt(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'user/auth'), format='json',
            data=self.auth_data)
        data = json.loads(response._container[0])
        return data.get('token')

    def test_get_list_unauthenticated(self):
        self.assertHttpUnauthorized(
            self.api_client.get(
                '{0}{1}'.format(API_PATH, 'book/'), format='json'))

    def test_get_list(self):
        token = self.get_jwt()
        response = self.api_client.get(
            '{0}{1}'.format(API_PATH, 'book/'),
            format='json',
            HTTP_AUTHORIZATION=token)
        self.assertHttpOK(response)

    def test_post_create_unauthorized(self):
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'book'), format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION='xxxxxxxxxxxx')
        self.assertHttpUnauthorized(response)

    def test_post_create(self):
        token = self.get_jwt()
        self.assertEqual(Book.objects.count(), 1)
        response = self.api_client.post(
            '{0}{1}'.format(API_PATH, 'book'),
            format='json',
            data=self.object_data,
            HTTP_AUTHORIZATION=token)
        self.assertHttpCreated(response)
        self.assertEqual(Book.objects.count(), 2)

    def test_post_update(self):
        token = self.get_jwt()
        self.api_client.put(
            '{0}{1}{2}'.format(API_PATH, 'book/', self.book.pk),
            format='json',
            data={'title': self.object_data.get('title')},
            HTTP_AUTHORIZATION=token)
        object_updated = Book.objects.get(id=self.book.pk)
        self.assertEqual(object_updated.title, 'python')
