from django.conf import settings


JWT_SETTINGS = {
    'SECRET': 'secret',
    'ALGORITHM': 'HS256',
    'EXPIRATION_TIME': 60 * 60 * 24
}

JWT_OVERRIDE_SETTINGS = getattr(settings, 'JWT_SETTINGS', {})

JWT_SETTINGS.update(JWT_OVERRIDE_SETTINGS)

globals().update(JWT_SETTINGS)
