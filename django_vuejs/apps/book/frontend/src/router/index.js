import Vue from 'vue'
import Router from 'vue-router'
import {store} from '../store.js'
import Admin from '@/components/Admin'
import Home from '@/components/Home'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  store,
  routes: [
    {
      path: '/admin',
      name: '',
      component: Admin,
      beforeEnter: (to, from, next) => {
        store.getters.isAuthenticated ? next() : next('login')
      },
      children: [
        {
          path: '',
          component: Home
        }
      ]
    },
    {
      path: '/',
      redirect: to => {
        return this.isAuthenticated ? 'admin' : 'login'
      }},
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: (to, from, next) => {
        store.getters.isAuthenticated ? next('admin') : next()
      }
    }
  ]
})
