import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import { VueAuthenticate } from 'vue-authenticate'
import axios from 'axios'
import router from './router'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const vueAuth = new VueAuthenticate(Vue.prototype.$http, {
  baseUrl: 'http://localhost:8000',
  apiPath: '/api/v1',
  tokenName: 'token',
  tokenPrefix: 'vueauth',
  tokenHeader: 'Authorization',
  tokenType: '',
  loginUrl: '/api/v1/user/auth',
  registerUrl: null,
  logoutUrl: null,
  storageType: 'localStorage',
  storageNamespace: 'auth',
  bindRequestInterceptor () {
    this.$http.interceptors.request.use((config) => {
      if (this.isAuthenticated()) {
        config.headers['Authorization'] = [
          this.options.tokenType, this.getToken()
        ].join(' ')
      } else {
        delete config.headers['Authorization']
      }
      return config
    })
  },
  bindResponseInterceptor () {
    this.$http.interceptors.response.use((response) => {
      this.setToken(response)
      return response
    })
  }
})

const store = new Vuex.Store({
  state: {
    isAuthenticated: !!localStorage.getItem('auth.vueauth_token'),
    currentUser: localStorage.getItem('auth.user'),
    isUnauthorized: false
  },
  getters: {
    isAuthenticated: state => {
      return state.isAuthenticated
    },
    isUnauthorized: state => {
      return state.isUnauthorized
    },
    getUser: state => {
      return state.currentUser
    }
  },
  mutations: {
    isAuthenticated: (state, payload) => {
      state.isAuthenticated = payload.isAuthenticated
    },
    isUnauthorized: (state, payload) => {
      state.isUnauthorized = payload.isUnauthorized
    },
    setUser: (state, payload) => {
      localStorage.setItem('auth.user', JSON.stringify(payload))
      state.currentUser = payload
    },
    logout: (state) => {
      localStorage.removeItem('auth.vueauth_token')
      localStorage.removeItem('auth.user')
      state.isUnauthorized = false
      state.isAuthenticated = false
      state.currentUser = {}
      router.push('login')
    }
  },
  actions: {
    login: (context, payload) => {
      vueAuth.login(payload.user, payload.requestOptions).then((response) => {
        context.commit('isAuthenticated', {
          isAuthenticated: vueAuth.isAuthenticated()
        })
        context.commit('setUser', response.data.user)
        context.commit('isUnauthorized', {
          isUnauthorized: false
        })
        router.push('admin')
      },
      () => {
        context.commit('isUnauthorized', {
          isUnauthorized: true
        })
      })
    },
    logout: (context) => {
      context.commit('logout')
    }
  }
})

export { store, vueAuth }
