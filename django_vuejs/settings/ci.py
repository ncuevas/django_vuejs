"""
Django settings for django_vuejs project.
Continuos Integration environment
"""
from .base import *

DATABASES['default'].update({
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': ':memory:',
    'USER': '',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '',
})
