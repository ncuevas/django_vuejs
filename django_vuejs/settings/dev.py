"""
Django settings for django_vuejs project.
Development environment
"""
from .base import *

INSTALLED_APPS += ('debug_toolbar',)

DEBUG = True
ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = (
    'http://localhost:8080'
)

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)

DATABASES['default'].update({
    'ENGINE': 'django.db.backends.postgresql',
    'NAME': 'django_vuejs',
    'USER': 'cegtest',
    'PASSWORD': 'cegtest',
    'HOST': 'postgres',
    'PORT': 5432,
})

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}
